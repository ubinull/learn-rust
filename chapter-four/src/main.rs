fn main() {
    variable_scope();
    the_string_type();
    memory_and_allocation();
    ownership_and_functions();
    returning_values_and_scope();
}

fn variable_scope() {
    let s = "Hello";
    {
        let s = "Hi";
        println!("{}", s);
    }
    println!("{}", s);
}

fn the_string_type() {
    let mut s = String::from("Hello");
    s.push_str(", World!");
    println!("{}", s);
}

fn memory_and_allocation() {
    let x = 5;
    let y = x;
    println!("x = {}, y = {}", x, y);

    // move
    let s1 = String::from("Hello");
    let s2 = s1;
    println!("s1 is invalidated, s2 = {}", s2);

    // clone (more expensive)
    let s1 = String::from("Hello");
    let s2 = s1.clone();
    println!("s1 = {}, s2 = {}", s1, s2);
}

fn ownership_and_functions() {
    let s = String::from("Hello");
    takes_ownership(s);

    let x = 5;
    makes_copy(x);

    fn takes_ownership(some_string: String) {
        println!("{}", some_string);
    }
    fn makes_copy(an_integer: i32) {
        println!("{}", an_integer)
    }
}

fn returning_values_and_scope() {
    let s1 = gives_ownership();
    let s2 = String::from("Hi");
    let s3 = takes_and_gives_back(s2);

    let s4 = String::from("Hi mom!");
    let (s5, len) = calculate_length(s4);
    println!("The length of '{}' is {}.", s5, len);

    fn gives_ownership() -> String {
        let string = String::from("Hello");
        string
    }

    fn takes_and_gives_back(another_string: String) -> String {
        another_string
    }

    fn calculate_length(s: String) -> (String, usize) {
        let length = s.len();
        (s, length)
    }
}

fn references_and_borrowing() {
    let s1 = String::from("Hello");
    let len = calculate_length(&s1);
    println!("The length of '{}' is {}", s1, len);

    fn calculate_length(s: &String) -> usize {
        s.len()
    }

    mutable_references();

    fn mutable_references() {
        let mut s = String::from("Hello");
        change(&mut s);
        println!("{}", s);

        fn change(some_string: &mut String) {
            some_string.push_str(", World!");
        }
    }
}

fn the_slice_type() {
    let s = String::from("Hello this is my Rust program");
    println!("{}", first_word(&s));

    fn first_word(s: &str) -> &str {
        let bytes = s.as_bytes();

        for (i, &item) in bytes.iter().enumerate() {
            if item == b' ' {
                return &s[0..i];
            }
        }
        &s[..]
    }
}
