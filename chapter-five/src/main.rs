fn main() {
    defining_and_instantiating_structs();
    an_example_program_using_structs();
    method_syntax();
}

fn defining_and_instantiating_structs() {
    struct User {
        username: String,
        email: String,
        password: String,
        created_at: String,
        paid: bool,
    }

    let my_user = User {
        paid: false,
        created_at: String::from("1690976690"),
        email: String::from("ubinull@pm.me"),
        password: String::from("abc123"),
        username: String::from("UbiOne"),
    };

    println!(
        "{} ({}) has payed for this service: {}",
        my_user.username, my_user.email, my_user.paid
    );

    let mut another_user = User {
        username: String::from("John"),
        email: String::from("john@example.com"),
        password: String::from("1234"),
        created_at: String::from("1659433490"),
        paid: true,
    };

    println!(
        "{}'s original password: {}",
        another_user.username, another_user.password
    );
    another_user.password = String::from("password1");
    println!(
        "{} changed their password to {}",
        another_user.username, another_user.password
    );

    let _some_user = create_user(
        String::from("George"),
        String::from("george.example@gmail.com"),
        String::from("qwertz987"),
        String::from("1690978135"),
    );

    fn create_user(
        username: String,
        email: String,
        password: String,
        current_time: String,
    ) -> User {
        User {
            username,
            email,
            password,
            created_at: current_time,
            paid: false,
        }
    }

    creating_instances_from_other_instances();
    using_tuple_structs();
    unit_like_structs();

    fn creating_instances_from_other_instances() {
        let random_user = create_user(
            String::from("George"),
            String::from("george.example@gmail.com"),
            String::from("qwertz987"),
            String::from("1690978135"),
        );

        let copycat_user = User {
            username: random_user.username,
            email: String::from("im.a@copycat.user"),
            password: random_user.password,
            created_at: random_user.created_at,
            paid: false,
        };

        let _another_copycat_user = User {
            email: String::from("more.easily@copy.someone"),
            ..copycat_user
        };
    }

    fn using_tuple_structs() {
        struct Colour(i32, i32, i32);
        struct Position(i32, i32, i32);

        let _black = Colour(0, 0, 0);
        let player = Position(340, 70, -987);

        println!(
            "Coordinates of player: X = {}, Y = {}, Z = {}",
            player.0, player.1, player.2
        );
    }

    fn unit_like_structs() {
        struct AlwaysEqual;
        let _subject = AlwaysEqual;
    }
}

fn an_example_program_using_structs() {
    let width1 = 30;
    let height1 = 50;

    println!(
        "The area of the rectangle is {} square pixels.",
        area1(width1, height1)
    );

    fn area1(width: u32, height: u32) -> u32 {
        width * height
    }

    let rect1 = (30, 50);

    println!(
        "The area of the rectangle is {} square pixels.",
        area2(rect1)
    );

    fn area2(dimensions: (u32, u32)) -> u32 {
        dimensions.0 * dimensions.1
    }

    #[derive(Debug)]
    struct Rectangle {
        width: u32,
        height: u32,
    }

    let scale = 2;
    let rect2 = Rectangle {
        width: dbg!(30 * scale),
        height: 50,
    };

    println!(
        "The area of the rectangle is {} sqare pixels.",
        area3(&rect2)
    );

    fn area3(rectangle: &Rectangle) -> u32 {
        rectangle.width * rectangle.height
    }

    println!("Our rectangle is: {:#?}", rect2);
    dbg!(&rect2);
}

fn method_syntax() {
    #[derive(Debug)]
    struct Rectangle {
        width: u32,
        height: u32,
    }

    impl Rectangle {
        fn area(&self) -> u32 {
            self.width * self.height
        }

        fn width(&self) -> bool {
            self.width > 0
        }

        fn can_hold(&self, rectangle: &Rectangle) -> bool {
            self.width * self.height >= rectangle.width * rectangle.height // "self.width > rectangle.width && self.height > rectangle.height" does the same thing
        }

        fn square(size: u32) -> Self {
            Self {
                width: size,
                height: size,
            }
        }
    }

    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };
    println!(
        "The area of the rectangle is {} sqare pixels.",
        rect1.area()
    );
    if rect1.width() {
        println!(
            "The rectangle has a nonzero width, which is: {}",
            rect1.width
        );
    }

    let rectangle1 = Rectangle {
        width: 30,
        height: 50,
    };
    let rectangle2 = Rectangle {
        width: 10,
        height: 40,
    };
    let rectangle3 = Rectangle {
        width: 60,
        height: 45,
    };

    println!(
        "Can rectangle1 hold rectangle2? {}",
        rectangle1.can_hold(&rectangle2)
    );
    println!(
        "Can rectangle1 hold rectangle3? {}",
        rectangle1.can_hold(&rectangle3)
    );

    println!("{:#?}", Rectangle::square(3));
}
