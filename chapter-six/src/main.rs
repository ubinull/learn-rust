fn main() {
    defining_an_enum();
    the_match_control_flow_construct();
    concise_control_flow_with_if_let();
}

fn defining_an_enum() {
    #[derive(Debug)]
    enum IpAddressVersion {
        V4,
        V6,
    }

    #[derive(Debug)]
    enum IpAddr {
        V4(u8, u8, u8, u8),
        V6(String),
    }

    #[derive(Debug)]
    struct IpAddress {
        version: IpAddressVersion,
        address: String,
    }

    impl IpAddr {
        fn println(&self) {
            println!("{:#?}", &self);
        }
    }

    let _four = IpAddressVersion::V4;
    let _six = IpAddressVersion::V6;

    fn route(ip_version: IpAddressVersion) {
        println!("{:#?}", ip_version);
    }

    route(IpAddressVersion::V4);
    route(IpAddressVersion::V6);

    let _home = IpAddress {
        version: IpAddressVersion::V4,
        address: String::from("192.168.0.177"),
    };

    let loopback = IpAddress {
        version: IpAddressVersion::V6,
        address: String::from("::1"),
    };

    println!("{:#?} {:#?}", loopback.version, loopback.address);

    let _localhost = IpAddr::V4(127, 0, 0, 1);
    let remote = IpAddr::V6(String::from("e68d:8cff:fecb:9377"));

    remote.println();

    the_option_enum();

    fn the_option_enum() {
        let _some_number = Some(5);
        let _some_char = Some('e');
        let _absent_number: Option<i32> = None;
    }
}

fn the_match_control_flow_construct() {
    #[derive(Debug)]
    enum State {
        Alabama,
        Ohio,
        Florida,
    }

    enum Coin {
        Penny,
        Nickel,
        Dime,
        Quarter(State),
    }

    fn value_in_cents(coin: Coin) -> u8 {
        match coin {
            Coin::Penny => 1,
            Coin::Nickel => 5,
            Coin::Dime => 10,
            Coin::Quarter(state) => {
                println!("State quarter from {:?}", state);
                25
            }
        }
    }

    println!("{}", value_in_cents(Coin::Penny));
    println!("{}", value_in_cents(Coin::Nickel));
    println!("{}", value_in_cents(Coin::Dime));

    println!("{}", value_in_cents(Coin::Quarter(State::Ohio)));
    println!("{}", value_in_cents(Coin::Quarter(State::Florida)));
    println!("{}", value_in_cents(Coin::Quarter(State::Alabama)));

    fn plus_one(x: Option<i32>) -> Option<i32> {
        match x {
            None => None,
            Some(x) => Some(x + 1),
        }
    }

    let five = plus_one(Some(4));
    let none = plus_one(None);

    println!("five = {:?}", five);
    println!("nothing there = {:?}", none);

    let roll = 9;

    match roll {
        7 => "You won!",
        2 => "You lost!",
        _other => "What is that??",
    };

    match roll {
        9 => "You won!",
        4 => "You lost!",
        _ => "Reroll! Now.",
    };
}

fn concise_control_flow_with_if_let() {
    #[derive(Debug)]
    enum State {
        Ohio,
    }

    enum Coin {
        Dime,
        Quarter(State),
    }

    let config_max: Option<u8> = Some(3);
    match config_max {
        Some(max) => println!("The maximum is configured to be {}", max),
        _ => (),
    }

    let config_max: Option<u8> = Some(5);
    if let Some(max) = config_max {
        println!("The maximum is configured to be {}", max);
    }

    let coin = Coin::Dime;
    let mut count = 0;
    match coin {
        Coin::Quarter(state) => println!("State quarter from {:?}", state),
        _ => count += 1,
    }
    println!("{}", count);

    let coin = Coin::Quarter(State::Ohio);
    let mut count = 0;
    if let Coin::Quarter(state) = coin {
        println!("State quarter from {:?}", state);
    } else {
        count += 1;
    }
    println!("{}", count);
}
