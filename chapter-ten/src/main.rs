fn largest(list: &[i32]) -> &i32 {
    let mut largest = &list[0];
    for number in list {
        if number > largest {
            largest = number;
        }
    }
    largest
}

fn main() {
    let numbers_list = vec![34, 51, 67, 22, 98, 172, 69];
    let result = largest(&numbers_list);
    println!("The largest number in list is: {}", result);

    let numbers_list = vec![44, 69, 420, 32, 18, 99];
    let result = largest(&numbers_list);
    println!("The largest number in the second list is: {}", result);
}
