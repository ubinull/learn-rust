fn main() {
    storing_lists_of_values_with_vectors();
    storing_utf8_encoded_text_with_strings();
    storing_keys_with_associated_values_in_hash_maps();
}

fn storing_lists_of_values_with_vectors() {
    creating_a_new_vector();
    updating_a_vector();
    reading_elements_of_vectors();
    iterating_over_the_values_in_a_vector();
    using_an_enum_to_store_multiple_types();

    fn creating_a_new_vector() {
        let _v: Vec<i32> = Vec::new();
        let _v = vec![1, 2, 3];
    }

    fn updating_a_vector() {
        let mut v = Vec::new();

        v.push(69);
        v.push(420);
    }

    fn reading_elements_of_vectors() {
        let v = vec![1, 2, 3, 4, 5];
        let _third = &v[2];

        let third = v.get(2);
        match third {
            Some(third) => println!("The third element is: {}", third),
            None => println!("There is no third element."),
        }
    }

    fn iterating_over_the_values_in_a_vector() {
        let v = vec![100, 32, 57];

        for i in &v {
            println!("{i}");
        }

        let mut v = vec![20, 40, 60, 100];

        for i in &mut v {
            *i += 50;
        }
        println!("{:?}", v);
    }

    fn using_an_enum_to_store_multiple_types() {
        #[derive(Debug)]
        enum SpreadsheetCell {
            Int(i64),
            Float(f64),
            Text(String),
        }

        let row = vec![
            SpreadsheetCell::Int(3),
            SpreadsheetCell::Float(69.420),
            SpreadsheetCell::Text(String::from("Hello, World!")),
        ];
        println!("{:?}", row);
    }
}

fn storing_utf8_encoded_text_with_strings() {
    creating_a_new_string();
    updating_a_string();
    concatenation();
    slicing_strings();
    methods_for_iterating_over_strings();

    fn creating_a_new_string() {
        let mut _s = String::new();

        let data = "initial contents";
        let _s = data.to_string();
        let _s = "initial contents".to_string();
        let _s = String::from("initial contents");
    }

    fn updating_a_string() {
        let mut s = String::from("Hello, ");
        s.push_str("World!");
        println!("{s}");

        let mut s1 = String::from("foo");
        let s2 = "bar";
        s1.push_str(s2);
        println!("s2 = {s2}");

        let mut s = String::from("h");
        s.push('i');
    }

    fn concatenation() {
        let s1 = String::from("Hello, ");
        let s2 = String::from("World!");
        let s3 = s1 + &s2;
        println!("{s3}");

        let s1 = String::from("tic");
        let s2 = String::from("tac");
        let s3 = String::from("toe");

        let s4 = s1 + "-" + &s2 + "-" + &s3;
        println!("{s4}");

        let s1 = String::from("tic");
        let s2 = String::from("tac");
        let s3 = String::from("toe");

        let s4 = format!("{s1}-{s2}-{s3}");
        println!("{s4}");
    }

    fn slicing_strings() {
        let hello = "Hello, World!";
        let s = &hello[0..2];
        println!("{s}");
    }

    fn methods_for_iterating_over_strings() {
        let hello = "Hello, World!";
        for char in hello.chars() {
            println!("{char}");
        }

        for bytes in hello.bytes() {
            println!("{bytes}");
        }
    }
}

fn storing_keys_with_associated_values_in_hash_maps() {
    creating_a_new_hash_map();
    accessing_values_in_a_hash_map();
    hash_maps_and_ownership();
    updating_a_hash_map();

    fn creating_a_new_hash_map() {
        use std::collections::HashMap;

        let mut scores = HashMap::new();
        scores.insert(String::from("blue"), 1);
        scores.insert(String::from("red"), 2);
    }

    fn accessing_values_in_a_hash_map() {
        use std::collections::HashMap;

        let mut scores = HashMap::new();
        scores.insert(String::from("blue"), 1);
        scores.insert(String::from("red"), 2);

        let team_name = String::from("red");
        let score = scores.get(&team_name).copied().unwrap_or(0);
        println!("{score}");

        for (key, value) in &scores {
            println!("{key}: {value}");
        }
    }

    fn hash_maps_and_ownership() {
        use std::collections::HashMap;

        let key = String::from("Name");
        let value = String::from("John");

        let mut map = HashMap::new();
        map.insert(&key, &value);

        println!("{key}: {value}");
    }

    fn updating_a_hash_map() {
        overwriting_a_value();
        adding_a_key_and_value_only_if_a_key_is_not_present();
        updating_a_value_based_on_the_old_value();

        fn overwriting_a_value() {
            use std::collections::HashMap;

            let mut scores = HashMap::new();
            scores.insert(String::from("Blue"), 10);
            scores.insert(String::from("Blue"), 20);

            println!("{:?}", scores);
        }

        fn adding_a_key_and_value_only_if_a_key_is_not_present() {
            use std::collections::HashMap;

            let mut scores = HashMap::new();
            scores.insert(String::from("Blue"), 3);
            scores.insert(String::from("Green"), 2);
            scores.insert(String::from("Yellow"), 5);

            scores.entry(String::from("Red")).or_insert(1);
            scores.entry(String::from("Green")).or_insert(69);
            println!("{:?}", scores);
        }

        fn updating_a_value_based_on_the_old_value() {
            use std::collections::HashMap;

            let text = "hello world wonderful world";
            let mut map = HashMap::new();

            for word in text.split_whitespace() {
                let count = map.entry(word).or_insert(0);
                *count += 1;
            }

            println!("{:?}", map);
        }
    }
}
