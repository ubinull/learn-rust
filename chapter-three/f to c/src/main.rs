use std::io;

fn main() {
    println!("Please enter fahrenheit to convert:");

    let mut fahrenheit = String::new();
    io::stdin()
        .read_line(&mut fahrenheit)
        .expect("Error: Failed to read line.");

    let fahrenheit: f64 = fahrenheit.trim().parse().expect("Please type in a number!");
    let celsius = (fahrenheit - 32.0) / 1.8;

    println!("{fahrenheit}˚F is ~{}˚C", celsius.round());
}
