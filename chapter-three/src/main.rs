fn main() {
    // variables_and_mutability();
    // data_types();
    // functions();
    // comments();
    control_flow();
}

fn _variables_and_mutability() {
    let mut x = 5;
    println!("The value of x is: {x}");

    x = 6;
    println!("The value of x is now: {x}");

    fn shadowing() {
        let x = 5;
        let x = x + 1;

        {
            let x = x * 2;
            println!("The value of x in the inner scope is: {x}");
            // a value is only shadowed until its scope ends
        }

        println!("The value of x is: {x}");

        // example use case: type conversion
        let spaces = "     ";
        let spaces = spaces.len();
        println!("{spaces}");
    }

    shadowing();
}

fn _data_types() {
    fn scalar_types() {
        fn integer_types() {
            let _x: i8 = 1;
            let _x: u8 = 1;
            let _x: i16 = 1;
            let _x: u16 = 1;
            let _x: i32 = 1;
            let _x: u32 = 1;
            let _x: i64 = 1;
            let _x: u64 = 1;
            let _x: i128 = 1;
            let _x: u128 = 1;
            let _x: isize = 1;
            let _x: usize = 1;

            fn number_literals() {
                let _decimal = 98_222;
                let _hex = 0xfff;
                let _octal = 0o77;
                let _binary = 0b1111_0000;
                let _byte: u8 = b'A'; // u8 ONLY!
            }

            number_literals();
        }

        fn floating_point_types() {
            let _x = 2.0;
            let _y: f32 = 3.0;
        }

        fn numeric_operations() {
            // addition
            let _sum = 5 + 10;

            // subtraction
            let _diff = 95.5 - 4.3;

            // multiplication
            let _product = 4 * 30;

            // division
            let _quotient = 56.7 / 32.2;
            let _truncated = -5 / 3;

            // remainder
            let _remainder = 43 % 5;
        }

        fn the_boolean_type() {
            let _t = true;
            let _f: bool = false;
        }

        fn the_character_type() {
            let _c = 'z';
            let _z: char = 'Z';
            let _nerd: char = '🤓';
        }

        integer_types();
        floating_point_types();
        numeric_operations();
        the_boolean_type();
        the_character_type();
    }

    fn compound_types() {
        fn the_tuple_type() {
            let tup: (i32, f64, u8) = (500, 6.4, 1);
            let (x, _y, _z) = tup;
            let y = tup.1;

            println!("The value of x is: {x}");
            println!("The value of y is: {y}");
        }

        fn the_array_type() {
            let months = [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December",
            ];
            let a: [i32; 5] = [1, 2, 3, 4, 5];
            let b = [3; 5];

            println!("{}", months[10]);
            println!("{}", a[4]);
            println!("{:?}", b);
        }

        the_tuple_type();
        the_array_type();
    }

    scalar_types();
    compound_types();
}

fn _functions() {
    fn example() {
        println!("Hello, World!");
        another_function();

        fn another_function() {
            println!("Hello from another function!");
        }
    }

    fn parameters() {
        another_function(6);
        sum(3, 4);

        fn another_function(x: i32) {
            println!("The value of x is: {x}");
        }

        fn sum(x: i32, y: i32) {
            println!("The sum of x and y is: {}", x + y);
        }
    }

    fn statements_and_expressions() {
        let _y = 6; // this is a statement
        let z = {
            let x = 3;
            x + 1
        };

        println!("The value of z is: {z}");
    }

    fn functions_with_return_values() {
        fn five() -> i32 {
            5
        }

        let x = five();
        println!("The value of x is: {x}");

        fn plus_one(x: i32) -> i32 {
            x + 1
        }

        let y = plus_one(5);
        println!("The value of y is: {y}");
    }

    example();
    parameters();
    statements_and_expressions();
    functions_with_return_values();
}

fn _comments() {
    // just a simple comment

    // multiple lines
    // of comments

    println!("Comments are amazing!"); // a comment placed in a line containing code

    // this piece of code prints hello world
    println!("Hello, World!");
}

fn control_flow() {
    fn branches() {
        let number = 12;

        if number < 5 {
            println!("Condition was true.");
        } else {
            println!("Condition was false.");
        }

        if number != 0 {
            println!("Number was something other than zero.");
        }

        if number % 4 == 0 {
            println!("Number is divisible by 4");
        } else if number % 3 == 0 {
            println!("Number is divisible by 3");
        } else if number % 2 == 0 {
            println!("Number is divisible by 2");
        } else {
            println!("Number is not divisible by 4, 3, or 2")
        }

        let condition = true;
        let number = if condition { 5 } else { 6 };
        println!("The value of number is: {number}");
    }

    fn loops() {
        let mut counter = 0;

        let result = loop {
            counter += 1;

            if counter == 10 {
                break counter * 2;
            }
        };

        println!("The result is {result}");

        let mut count = 0;
        'counting_up: loop {
            println!("count = {count}");
            let mut remaining = 10;

            loop {
                println!("remaining = {remaining}");
                if remaining == 9 {
                    break;
                }
                if count == 2 {
                    break 'counting_up;
                }
                remaining -= 1;
            }
            count += 1;
        }
        println!("End count = {count}");

        let mut number = 3;

        while number != 0 {
            println!("{number}");

            number -= 1;
        }

        println!("Aaaaand... LIFTOFF!!!");

        let a = [10, 20, 30, 40, 50];
        let mut index = 0;

        while index < 5 {
            println!("The value is: {}", a[index]);
            index += 1;
        }

        for element in a {
            println!("The value is: {element}");
        }

        for num in (1..4).rev() {
            println!("{num}");
        }
        println!("LIFTOFF!");
    }

    branches();
    loops();
}
