use std::io;

fn main() {
    let mut last = 0;
    let mut current = 1;
    let mut i = 0;
    let mut n = String::new();

    println!("Type in a number:");
    io::stdin()
        .read_line(&mut n)
        .expect("Error: Failed to read line.");
    let n: u64 = n.trim().parse().expect("Please type in a number.");

    if n == 0 {
        panic!("There is no such thing as the 0th fibonacci number!");
    }

    while i != n - 2 {
        let temp = last + current;
        last = current;
        current = temp;

        i += 1;
    }

    println!("The {n}. fibonacci number is: {current}");
}
